{
	//Transport step
	Info << "transport step" << endl;
    while (simple.correctNonOrthogonal())
    {
        forAll(solutionSpecies, i)
		{
			volScalarField& Yi = Y[i];
			dimensionedScalar DYi = DY[i];

			fvScalarMatrix YiEqn
			(
				fvm::ddt(Yi)
				+ fvm::div(phi, Yi, "div(phi,Yi)")
				- fvm::laplacian(DYi, Yi)
			);

			YiEqn.solve(mesh.solutionDict().solver("Yi"));
		}
	}

    //reaction step
	int num_type = 0;
	forAll(solutionSpecies, i)
	{
		volScalarField& conc_i = Y[i];
		for(int j = 0; j < ncells; ++j)
		{
			initial_conc_i[num_type][j] = conc_i[j];
		}
		num_type += 1;
	}
    Info << "reaction step" << endl;
#	include "CoupledReaction.H"
}
