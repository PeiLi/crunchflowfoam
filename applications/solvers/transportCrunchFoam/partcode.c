    forAll(mesh.boundaryMesh(), patchI)
    {
        string patchNameInDomain = "Patch name in block mesh";
        string patchName = mesh.boundaryMesh()[patchI].name();
        Info << "The patch name is " << patchName << endl;
        for(int i = 0; i < patchesNameList.size(); ++i)
        {
            string nameInList = patchesNameList[i];
            if (nameInList == patchName)
            {
                label patchID = mesh.boundaryMesh().findPatchID(patchName);
                const fvPatch& cPatch = mesh.boundary()[patchID];
                forAll(cPatch, faceI)
                {
                    int num_species = 0;
                    forAll(solutionSpecies, j)
                    {
                        Info << Y[j].boundaryField()[patchID][faceI] << endl;
                        //scalar tmp_bc = Y[i].boundaryField()[patchID][faceI];
                        Info << "The boundary condition value from CrunchFlow input file: " << chem_bc_state[i].total_mobile.data[num_species] << endl;
                        Y[j].boundaryField()[patchID][faceI] = chem_bc_state[i].total_mobile.data[num_species];

                        Info << "The boundary condition in the OpenFoam now is: " << Y[j].boundaryField()[patchID][faceI] << endl;
                        num_species += 1;
                    }
                }
            }
        }
    }
    //Copy the initial values back to openFOAM
    int num_type = 0;
    forAll(solutionSpecies, i)
    {
		// volScalarField& conc_i = Y[i];
		for(int j = 0; j < ncells; ++j)
		{
			Y[i][j] = initial_conc_i[num_type][j];
		}
        // Y[i] = conc_i;
		num_type += 1;
   }
