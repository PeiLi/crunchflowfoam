/*---------------------------------------------------------------------------*\

License
    This file is part of GeoChemFoam, an Open source software using OpenFOAM
    for multiphase multicomponent reactive transport simulation in pore-scale
    geological domain.

    GeoChemFoam is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version. See <http://www.gnu.org/licenses/>.

    The code was developed by Dr Julien Maes as part of his research work for
    the Carbonate Reservoir Group at Heriot-Watt University. Please visit our
    website for more information <https://carbonates.hw.ac.uk>.

Application
    multiSpeciesTransportFoam

Description
    Solves a transport equation with multiple passive species

\*---------------------------------------------------------------------------*/
#include <cmath>
#include "petsc.h"
#include "fvCFD.H"
#include "fvPatch.H"
#include "simpleControl.H"
#include "speciesTable.H"
#include <string>
#include <cfloat>
#include <climits>
#include <cstdlib>

#include "alquimia_containers.h"
#include "alquimia_interface.h"
#include "alquimia_memory.h"
#include "alquimia_util.h"




// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"

#   include "createTime.H"
#   include "createMesh.H"

    simpleControl simple(mesh);

//Get boundary patch names

#   include "createFields.H"
//output the number of cells in the domain from OpenFOAM
int ncells = mesh.cells().size();
Info << "The number of cells in the domain is " << ncells << endl;
//Output the cell volume in the domain from OpenFOAM
// forAll(mesh.cells(), cellI)
// {
//     Info << "The volume of each cell is " << mesh.V()[cellI] << endl;
// }
//Output dx in 2D cell
double* delta_x = new double[ncells];
const surfaceVectorField& center = mesh.Cf();
delta_x[0] = center[0].x();
double half_delta_x = center[0].x();
for (int i = 1; i < ncells; ++i)
{
    double distance_center = center[i].x() - center[i - 1].x();
    delta_x[i] = distance_center;
}
//Output initial tracer concentration from OpenFOAM
double** initial_conc_i = new double* [solutionSpecies.size()];
for (int i = 0; i < solutionSpecies.size(); ++i)
{
    initial_conc_i[i] = new double [ncells];
}
int num_total = 0;
forAll(solutionSpecies, i)
{
    volScalarField& conc_i = Y[i];
    for(int j = 0; j < ncells; ++j)
    {
        initial_conc_i[num_total][j] = conc_i[j];
    }
    num_total += 1;
}

#   include "createAlquimia.H"

#   include "initializeCrunchFlow.H"



// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nCalculating scalar transport\n" << endl;

#   include "CourantNo.H"
    while (simple.loop())
    {
        Info<< "Time = " << runTime.timeName() << nl << endl;
        double deltaT = runTime.deltaT().value(); 
        Info << "The time step is " << deltaT << "s." << endl; 
#       include "YiEqn.H"
        //forAll(solutionSpecies, i)
        //{
        //    for (int j = 0; j < ncells; ++j)
          //  {
            //    Y[i][j] = Y[i][j] / 1000.;
           // }
       // }
        runTime.write();
        //forAll(solutionSpecies, i)
        //{
        //    for (int j = 0; j < ncells; ++j)
          //  {
          //      Y[i][j] = Y[i][j] * 1000.;
          //  }
       // }
    }
#   include "FreeAlquimia.H"

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
