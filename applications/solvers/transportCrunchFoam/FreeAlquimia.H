    /*Delete parameters of Alquimia for using CrunchFlow*/
    Info << "Delete parameters for Alquimia and shut it down." << endl;
    delete advective_fluxes;
    FreeAlquimiaState(&advected_chem_state);
    FreeAlquimiaAuxiliaryData(&advected_chem_aux_data);

    //Destroy boundary and initial conditions
    //if (&chem_inlet_bc_alquimia.name != NULL)
   // {
   //     FreeAlquimiaGeochemicalCondition(&chem_inlet_bc_alquimia);
   // }
// FreeAlquimiaState(&chem_inlet_state);
//    FreeAlquimiaAuxiliaryData(&chem_inlet_aux_data);
//    if (&chem_outlet_bc_alquimia.name != NULL)
//    {
//        FreeAlquimiaGeochemicalCondition(&chem_outlet_bc_alquimia);
//    }
//    FreeAlquimiaState(&chem_outlet_state);
//    FreeAlquimiaAuxiliaryData(&chem_outlet_aux_data);
//    FreeAlquimiaGeochemicalCondition(&chem_ic_alquimia);

    //Destroy chemistry data
    for (int i = 0; i < icInCrunchFlow.size(); ++i)
    {
        if (&chem_ic_geochem[i].name != NULL)
        {
            FreeAlquimiaGeochemicalCondition(&chem_ic_geochem[i]);
        }
    }

    for (int i = 0; i < bcInCrunchFlow.size(); ++i)
    {
        if (&chem_bc_geochem[i].name != NULL)
        {
            FreeAlquimiaGeochemicalCondition(&chem_bc_geochem[i]);
        }
        FreeAlquimiaState(&chem_bc_state[i]);
        FreeAlquimiaAuxiliaryData(&chem_aux_bc_data[i]);
    }
    delete [] chem_bc_geochem;
    delete [] chem_bc_state;
    delete [] chem_aux_bc_data;
    delete [] chem_ic_geochem;
    //Destroy array for transferring data between OpenFOAM and CrunchFlow
    for (int i = 0; i < solutionSpecies.size(); ++i)
    {
        delete [] initial_conc_i[i];
    }
 
    for (int i = 0; i < ncells; ++i)
    {
        FreeAlquimiaState(&chem_state[i]);
        FreeAlquimiaProperties(&chem_properties[i]);
        FreeAlquimiaAuxiliaryData(&chem_aux_data[i]);
    }
    delete [] chem_state;
    delete [] chem_properties;
    delete [] chem_aux_data;
    FreeAlquimiaProblemMetaData(&chem_metadata);

    //Destroy chemistry engine
    chem.Shutdown(&chem_engine_alquimia, &chem_status);
    FreeAlquimiaEngineStatus(&chem_status);

    delete [] initial_conc_i;
    PetscInt petsc_error = PetscFinalize();
